package azure_storage_example;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.SparkSession;

public class SparkWithAzureStorageExample {
	public static void main(String[] args) {
		SparkConf config = new SparkConf();
		config.setMaster("local[*]");
		config.setAppName("app");

		/* WASB */
		config.set("spark.hadoop.fs.wasb.impl", org.apache.hadoop.fs.azure.NativeAzureFileSystem.class.getName());

		/*The storage account name has to be added in the key name, and the value is one of the keys under 'Access Keys'
		  in the Azure Storage Accounts section of the Azure Portal*/
		config.set("fs.azure.account.key.[storage-account-name].blob.core.windows.net", "[access-key]");

		/* ADL */
		config.set("spark.hadoop.fs.adl.impl", org.apache.hadoop.fs.adl.AdlFileSystem.class.getName());
		// Don't know if this is required, but it doesn't hurt
		config.set("spark.fs.AbstractFileSystem.adl.impl", org.apache.hadoop.fs.adl.Adl.class.getName());

		config.set("fs.adl.oauth2.access.token.provider.type", "ClientCredential");

		/* Client ID is generally the application ID from the azure portal app registrations*/
		config.set("fs.adl.oauth2.client.id", "[client-id]");

		/*The client secret is the key generated through the portal*/
		config.set("fs.adl.oauth2.credential", "[client-secret]");

		/*This is the OAUTH 2.0 TOKEN ENDPOINT under the ENDPOINTS section of the app registrations under Azure Active Directory*/
		config.set("fs.adl.oauth2.refresh.url", "[oauth-2.0-token-endpoint]");

		SparkSession spark = new SparkSession(new SparkContext(config));

		spark.read().csv("wasb://[container-name]@[storage-account-name].blob.core.windows.net/[path]").show(false);
		spark.read().parquet("adl://[azure-data-lake-name].azuredatalakestore.net/[path]").show(false);

		spark.close();
	}
}
