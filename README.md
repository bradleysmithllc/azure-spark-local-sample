# azure-spark-local-sample

This project is a simple, minimal example of how to configure an embedded spark application to use
the storage accounts in Microsoft Azure.  WASB or ADL or both can be used.

This same approach will work with standalone clusters as well.  It is built for spark 2.3.0 and hadoop 3.1.0.

I don't know if it will work with other versions.  This project MUST be used with Java 8.

# Spark Scheme's
Spark uses context properties to determine which filesystem provider handles which type of
url schemes.  The property spark.hadoop.fs.wasb.impl tells spark to use the provided
filesystem class whenever a url starting with wasb:// is encountered.